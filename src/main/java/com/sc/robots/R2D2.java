package com.sc.robots;

import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Robot;

public class R2D2 {

	private static final long CHECK_INTERVAL = 100l;
	private static final int STATE_WAITING = 1;
	private static final int STATE_ACTIVATED = 2;
	private static final int PIXEL_DELTA = 12;
	private static final int PIXEL_OFFSET = 6;

	public static void main(String[] args) {
		long delay;
		try {
			delay = Long.parseLong(args[0]);
		} catch(Exception e) {
			delay = 5000l;
		}
		try {
			Robot robot = new Robot();
			robot.setAutoWaitForIdle(true);
			robot.setAutoDelay(0);
			float state = STATE_WAITING;
			
			Point lastLocation = MouseInfo.getPointerInfo().getLocation();
			long lastDetection = System.currentTimeMillis();
			
			System.out.println("C3PO: R2D2, what are you doing?");
			while(true) {
				if(STATE_WAITING==state) {
					if(!lastLocation.equals(MouseInfo.getPointerInfo().getLocation())) {
						lastLocation = MouseInfo.getPointerInfo().getLocation();
						lastDetection = System.currentTimeMillis();
						try {
							Thread.sleep(CHECK_INTERVAL);
						} catch(InterruptedException e) {}
					} else if(lastDetection + delay < System.currentTimeMillis()) {
						System.out.println("R2D2: Beep bop beep!");
						state = STATE_ACTIVATED;
					} else {
						try {
							Thread.sleep(CHECK_INTERVAL);
						} catch(InterruptedException e) {}
					}
				} else {
					if(!lastLocation.equals(MouseInfo.getPointerInfo().getLocation())) {
						System.out.println("R2D2: Uiiiiiiii");
						state = STATE_WAITING;
					} else {
						int offsetX = (int)(PIXEL_OFFSET-Math.random()*PIXEL_DELTA);
						int offsetY = (int)(PIXEL_OFFSET-Math.random()*PIXEL_DELTA);
						double newX = lastLocation.getX()+offsetX;
						double newY = lastLocation.getY()+offsetY;
						while(lastLocation.getX()==newX && lastLocation.getY()==newY) {
							offsetX = (int)(PIXEL_OFFSET-Math.random()*PIXEL_DELTA);
							offsetY = (int)(PIXEL_OFFSET-Math.random()*PIXEL_DELTA);
							newX = lastLocation.getX()+offsetX;
							newY = lastLocation.getY()+offsetY;
						}
						robot.mouseMove((int)newX, (int)newY);
						lastLocation = MouseInfo.getPointerInfo().getLocation();
						lastDetection = System.currentTimeMillis();
						try {
							Thread.sleep(CHECK_INTERVAL);
						} catch(InterruptedException e) {}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
